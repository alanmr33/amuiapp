package com.clumsia.amui;

import android.app.Application;
import com.facebook.stetho.Stetho;

/**
 * Created by Indocyber on 15/12/2017.
 */

public class AMApp extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
