package com.clumsia.amui.handler;

/**
 * Created by Indocyber on 15/12/2017.
 */

public interface OnSetValue {
    void OnSet(String value);
}
