package com.clumsia.amui.library;

import android.content.Context;

import com.facebook.android.crypto.keychain.AndroidConceal;
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.CryptoConfig;
import com.facebook.crypto.Entity;
import com.facebook.crypto.exception.CryptoInitializationException;
import com.facebook.crypto.exception.KeyChainException;
import com.facebook.crypto.keychain.KeyChain;

import org.apache.commons.io.IOUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Indocyber on 15/12/2017.
 */

abstract class AMFileManager {
    public static void writeFile(Context ctx, String filename, byte[] body){
        KeyChain keyChain = new SharedPrefsBackedKeyChain(ctx, CryptoConfig.KEY_256);
        Crypto crypto = AndroidConceal.get().createDefaultCrypto(keyChain);
        if (!crypto.isAvailable()) {
            return;
        }
        File storageDir = ctx.getExternalFilesDir("secure");
        File file = null;
        try {
            file = new File(storageDir.getAbsolutePath()+"/"+filename);
            if(!file.exists()){
                file.createNewFile();
            }
            OutputStream fileStream = new BufferedOutputStream(
                    new FileOutputStream(file));
            OutputStream outputStream = crypto.getCipherOutputStream(
                    fileStream,
                    Entity.create(filename));
            outputStream.write(body);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyChainException e) {
            e.printStackTrace();
        } catch (CryptoInitializationException e) {
            e.printStackTrace();
        }
    }
    public static byte[] readFile(Context ctx,String filename){
        KeyChain keyChain = new SharedPrefsBackedKeyChain(ctx, CryptoConfig.KEY_256);
        Crypto crypto = AndroidConceal.get().createDefaultCrypto(keyChain);
        if (!crypto.isAvailable()) {
            return null;
        }
        File storageDir = ctx.getExternalFilesDir("secure");
        File file = null;
        try {
            file = new File(storageDir.getAbsolutePath()+"/"+filename);
            if(!file.exists()){
                file.createNewFile();
            }
            FileInputStream fileStream = new FileInputStream(file);
            InputStream inputStream = crypto.getCipherInputStream(
                    fileStream,
                    Entity.create(filename));
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyChainException e) {
            e.printStackTrace();
        } catch (CryptoInitializationException e) {
            e.printStackTrace();
        }
        return null;
    }
}
