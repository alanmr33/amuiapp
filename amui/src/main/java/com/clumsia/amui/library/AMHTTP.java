package com.clumsia.amui.library;

import android.content.ContentValues;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;
import java.util.Set;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Indocyber on 15/12/2017.
 */

abstract class AMHTTP {
    public static String get(String url , ContentValues values){
        OkHttpClient client = new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
        Request.Builder builder=new Request.Builder();
        if(!url.contains("?")){
            url+="?";
        }
        Set<String> keys=values.keySet();
        for (String k:keys) {
            url+=k+"="+values.getAsString(k)+"&";
        }
        builder.url(url);
        Request request = builder.build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static byte[] getByte(String url , ContentValues values){
        OkHttpClient client = new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
        Request.Builder builder=new Request.Builder();
        if(!url.contains("?")){
            url+="?";
        }
        Set<String> keys=values.keySet();
        for (String k:keys) {
            url+=k+"="+values.getAsString(k)+"&";
        }
        builder.url(url);
        Request request = builder.build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().bytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static String post(String url,ContentValues values){
        OkHttpClient client = new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
        FormBody.Builder builder=new FormBody.Builder();
        Set<String> keys=values.keySet();
        for (String k:keys) {
            builder.add(k,values.getAsString(k));
        }
        RequestBody body = builder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static byte[] postByte(String url,ContentValues values){
        OkHttpClient client = new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
        FormBody.Builder builder=new FormBody.Builder();
        Set<String> keys=values.keySet();
        for (String k:keys) {
            builder.add(k,values.getAsString(k));
        }
        RequestBody body = builder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().bytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
